#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

echo "Your installed Kernels:"
dpkg --get-selections | grep '^linux-headers-.*-generic' | sed -r 's/^linux-headers-(.+)-generic.*/\1/g'

version=`uname -r | sed -r 's/-generic//g'`
printf "You are using Kernel verion \e[1;92m${version}\e[0m currently.\n"

echo "Listing available Kernels below:"
apt-cache search '^linux-headers-.+-generic' | sed -r 's/^linux-headers-(.+)-generic.*/\1/g'

echo -n "Which Kernel version you would to update ? "
read v
if [ "${v}" = "" ]; then
	echo "Installing Kernel terminated"
else
	packages=`dpkg --get-selections | grep "${version}" | awk -F ' ' '{print $1}' | sed -r "s/${version}/${v}/g" | tr '\n' ' '`
	command="sudo apt-get -y install ${packages}"
	echo "${command}"
	eval "${command}"
fi

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

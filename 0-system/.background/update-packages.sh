#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

for command in 'update' 'upgrade' 'dist-upgrade' 'autoremove' 'autoclean' 'clean'; do
	command="sudo apt-get -y ${command}"
	echo "${command}"
	eval "${command}"
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

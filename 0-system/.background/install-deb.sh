#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

for f in `echo "${1}"`; do
	if [ "${#f}" -gt '0' ]; then
		command="dpkg -i '${f}'"
		echo "${command}"
		eval "${command}"
	fi
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

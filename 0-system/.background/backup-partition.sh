#!/bin/bash

TEMP="temp"
WIDTH="300"
HEIGHT="200"
BUFFER="512"
SPLIT="2G"

while [ "1" ]; do
	info=`lsblk -l -o "NAME,SIZE" | grep "^sd[a-z]\\+ "`
	drive=`zenity --list --title="Select a drive" --text="<big>Select a drive to backup</big>" --width="${WIDTH}" --height="${HEIGHT}" --column "Drive" --column "Size" $info 2>"/dev/null"`
	error="${?}"
	if [ "${error}" = "1" ]; then
		break
	elif [ "${drive}" = "" ]; then
		zenity --info --title="Caution" --text="<big>Select a drive</big>" --width="${WIDTH}" 2>"/dev/null"
	else
		while [ "1" ]; do
			info=`lsblk -l -o "NAME,SIZE" | grep "^${drive}[0-9]\\+ " | tr -d "${drive}"`
			partition=`zenity --list --title="Select a partition" --text="<big>Select a partition to backup</big>" --width="${WIDTH}" --height="${HEIGHT}" --column "Drive" --column "Size" $info 2>"/dev/null"`
			error="${?}"
			if [ "${error}" = "1" ]; then
				break
			elif [ "${drive}" = "" ]; then
				zenity --info --title="Caution" --text="<big>Select a partition</big>" --width="${WIDTH}" 2>"/dev/null"
			else
				path=`zenity --file-selection --title="Select backup path" --directory 2>"/dev/null"`
				error="${?}"
				if [ "${error}" = "1" ]; then
					break
				else
					name="${drive}${partition}-"`date "+%Y-%m-%d"`
					sh="${name}.sh"
					img="${name}.img"
					bz2="${img}.bz2"
					mkdir "${path}/${TEMP}"
					cd "${path}/${TEMP}"
					echo "Backup, compress and split '/dev/${drive}${partition}' ("`lsblk -l -o "NAME,SIZE" | grep "^${drive}${partition} " | awk -F ' ' '{print $2}'`") to '${path}'."
					dd if="/dev/${drive}${partition}" bs="${BUFFER}" status="progress" | bzip2 -9 | split -b"${SPLIT}"
					for f in "x"*; do
						mv "${f}" "../${bz2}.${f}"
					done
					cd ".."
					rmdir "${TEMP}"
					echo "Backup completed."

					printf "#!/bin/bash\n" >"${sh}"
					printf "\ndrive=\"${drive}\"" >>"${sh}"
					printf "\npartition=\"${partition}\"" >>"${sh}"
					printf "\nbz2=\"${bz2}\"" >>"${sh}"
					printf "\n" >>"${sh}"
					printf "\necho \"Combine, uncompress and restore '\${bz2}.x*' to '/dev/${drive}${partition}'.\"" >>"${sh}"
					printf "\ncat \"\${bz2}.x\"* | bunzip2 | dd of=\"/dev/\${drive}\${partition}\" bs=\"${BUFFER}\" status=\"progress\"" >>"${sh}"
					printf "\necho \"Restore completed.\"" >>"${sh}"

					break
				fi
			fi
		done
	fi
done

#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

echoeval(){
	echo "${1}"
	eval "${1}"
}

echoeval "echo 'DPKG::Progress-Fancy 1;' >'/etc/apt/apt.conf.d/99progressfancy'"

apt-get update
apt-get -y install curl wget

echoeval "echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb stable main' >'/etc/apt/sources.list.d/google-chrome.list'"
echoeval "echo 'deb http://download.webmin.com/download/repository sarge contrib' >'/etc/apt/sources.list.d/webmin.list'"
echoeval "echo 'deb http://linux.teamviewer.com/deb stable main' >'/etc/apt/sources.list.d/teamviewer.list'"
echoeval "echo 'deb http://dl.bintray.com/dawidd6/neofetch jessie main' >'/etc/apt/sources.list.d/neofetch.list'"

for url in \
	"https://dl.google.com/linux/linux_signing_key.pub" \
	"http://www.webmin.com/jcameron-key.asc" \
	"https://dl.tvcdn.de/download/linux/signature/TeamViewer2017.asc" \
	"https://bintray.com/user/downloadSubjectPublicKey?username=bintray" \
; do
	echoeval "curl -sL '${url}' | apt-key add"
done

for ppa in \
	"obsproject/obs-studio" \
	"ubuntuhandbook1/avidemux" \
	"libretro/stable" \
	"libretro/testing" \
; do
	echoeval "add-apt-repository -y 'ppa:${ppa}'"
done

apt-get update

packages=""
packages="${packages} vim gedit pavucontrol"
packages="${packages} libreoffice"
packages="${packages} git webmin transmission-gtk neofetch"
#packages="${packages} teamviewer"
packages="${packages} firefox chromium-browser google-chrome-stable"
packages="${packages} avidemux2.7-qt5 dia ffmpeg gimp imagemagick inkscape python-lxml python-scour obs-studio"
packages="${packages} ibus-table-cangjie3"
#packages="${packages} apache2 php sqlite php-sqlite3 mysql-server php-mysql phpmyadmin"
#packages="${packages} libretro-beetle-psx pcsx2 steam gameconqueror"
packages="${packages} samba samba-vfs-modules smbclient"
packages="${packages} cups"
#packages="${packages} sendmail telnet"
#packages="${packages} proftpd ftp filezilla"
#packages="${packages} virtualbox-qt virtualbox-guest-additions-iso"
echoeval "apt-get -y install ${packages}"

filename="AdbeRdr9.5.5-1_i386linux_enu.deb"
echoeval "wget 'ftp://ftp-pac.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/${filename}'"
echoeval "dpkg -i '${filename}'"
echoeval "apt-get -y -f install"
echoeval "rm '${filename}'"
echoeval "chown -R root:root '/opt/Adobe'"

for command in 'autoremove' 'autoclean' 'clean'; do
	echoeval "apt-get -y ${command}"
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

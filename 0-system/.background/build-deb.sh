#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

for f in `echo "${1}"`; do
	if [ "${#f}" -gt '0' ]; then
		package=`basename "${f}" | sed -r 's/^(.+)-(.+)$/\1/g'`
		version=`basename "${f}" | sed -r 's/^(.+)-(.+)$/\2/g'`
		DEBIAN="${f}/DEBIAN"
		control="${DEBIAN}/control"
		total=`du -b "${f}" | tail -1 | awk -F ' ' '{print $1}'`
		size=$(($total/1024))
		if ! [ -d "${DEBIAN}" ]; then
			mkdir "${f}/DEBIAN" 2>/dev/null
			echo "Package: ${package}" >"${control}"
			echo "Version: ${version}" >>"${control}"
			echo "Maintainer: maintainer" >>"${control}"
			echo "Priority: optional" >>"${control}"
			echo "Architecture: all" >>"${control}"
			echo "Section: any" >>"${control}"
			echo "Installed-Size: ${size}" >>"${control}"
			echo "Description: some description" >>"${control}"
			echo "" >>"${control}"
		fi
		command="sudo chown -R 'root:root' '${f}'"
		echo "${command}"
		eval "${command}"
		command="dpkg -b '${f}'"
		echo "${command}"
		eval "${command}"
		command="sudo chown -R '${USER}:${USER}' '${f}'"
		echo "${command}"
		eval "${command}"
	fi
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

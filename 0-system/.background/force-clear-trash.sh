#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

command="rm -R -v '${HOME}/.local/share/Trash/files/'*"
echo "${command}"
eval "${command}"

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

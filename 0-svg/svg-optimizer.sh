#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

decimal_places="6" # length of decimal places
simple_color="0" # change color value as #RGB instead of #RRGGBB or color name
xml_attribute="1" # change style value to XML attribute
remove_group="0" # remove group without any attribute
group_similar="0" # create group to the similar elements
remove_banner="1" # remove XML namespace elements such as Inkscape, Sodipodi, Adobe...
remove_unref_defs="1" # remove unreference elements in defs element
xml_header="0" # output with XML header
remove_title="1" # remove titls elements
remove_description="1" # remove description elements
remove_metadata="1" # remove metadata elements
remove_comment="1" # remove XML comment elements
remove_base64="0" # remove base64 data
use_viewbox="1" # use "viewBox" attribute
space_type="tab" # indentation type: none, space, tab
spaces="1" # indentation value
has_linebreak="1" # XML with linebreaks
remove_preserve="1" # remove "preserve" elements
#remove_unref_ids="1" # remove unreference IDs
#keep_ids_prefix="protected-" # keep IDs with specific prefixes

keep_ids_prefix=`zenity \
--width="300" \
--title="SVG Optimizer" \
--text="Initial Settings" \
--add-entry="Prefix of Protected ID (Default: \"protected-\")" \
--separator=" " \
--forms`

if [ "${?}" = "0" ]; then
	remove_unref_ids="1"
	if [ "${keep_ids_prefix}" = "" ]; then
		keep_ids_prefix="protected-"
	fi
else
	remove_unref_ids="0"
	keep_ids_prefix=""
fi

options=""
options="${options} --set-precision='${decimal_places}'"
options="${options} --indent='${space_type}'"
options="${options} --nindent='${spaces}'"
if [ "${simple_color}" = "0" ]; then
	options="${options} --disable-simplify-colors"
fi
if [ "${xml_attribute}" = "0" ]; then
	options="${options} --disable-style-to-xml"
fi
if [ "${remove_group}" = "0" ]; then
	options="${options} --disable-group-collapsing"
fi
if [ "${group_similar}" = "1" ]; then
	options="${options} --create-groups"
fi
if [ "${remove_banner}" = "0" ]; then
	options="${options} --keep-editor-data"
fi
if [ "${remove_unref_defs}" = "0" ]; then
	options="${options} --keep-unreferenced-defs"
fi
if [ "${xml_header}" = "0" ]; then
	options="${options} --strip-xml-prolog"
fi
if [ "${remove_title}" = "1" ]; then
	options="${options} --remove-titles"
fi
if [ "${remove_description}" = "1" ]; then
	options="${options} --remove-descriptions"
fi
if [ "${remove_metadata}" = "1" ]; then
	options="${options} --remove-metadata"
fi
if [ "${remove_comment}" = "1" ]; then
	options="${options} --enable-comment-stripping"
fi
if [ "${remove_base64}" = "1" ]; then
	options="${options} --disable-embed-rasters"
fi
if [ "${use_viewbox}" = "1" ]; then
	options="${options} --enable-viewboxing"
fi
if [ "${has_linebreak}" = "0" ]; then
	options="${options} --no-line-breaks"
fi
if [ "${remove_preserve}" = "1" ]; then
	options="${options} --strip-xml-space"
fi
if [ "${remove_unref_ids}" = "1" ]; then
	options="${options} --enable-id-stripping"
fi
if ! [ "${keep_ids_prefix}" = "" ]; then
	options="${options} --protect-ids-prefix='${keep_ids_prefix}'"
fi

dir="temp"
mkdir "${dir}"

i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		b=`basename "${f}"`
		command="scour ${options} -i '${f}' -o '${dir}/${b}'"
		echo "# ${command}"
		eval "${command}"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="SVG Optimizer" --progress
#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

size=`zenity \
--width="300" \
--title="SVG to PNG" \
--text="Initial Settings" \
--add-entry="Width" \
--add-entry="Height" \
--forms`

dir="temp"
mkdir "${dir}"

i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		width=`echo "${size}" | awk -F '|' '{print $1}'`
		height=`echo "${size}" | awk -F '|' '{print $2}'`
		b=`basename "${f}"`
		command="inkscape --export-area-page --export-width='${width}' --export-height='${height}' --export-type='png' --export-filename='${dir}/${b:0:-4}-${width}x${height}.png' '${f}'"
		echo "# ${command}"
		eval "${command}"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="SVG to PNG" --progress
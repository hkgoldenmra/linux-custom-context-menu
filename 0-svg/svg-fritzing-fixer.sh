#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'
dir="temp"
mkdir "${dir}"

for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		b=`basename "${f}"`
		command="cat '${f}' | sed -r 's/([^ ]) +([^ ])/\1 \2/g' | sed -r 's/svg='\"'\"'http:\/\/www.w3.org\/2000\/svg'\"'\"'/xlink='\"'\"'http:\/\/www.w3.org\/1999\/xlink'\"'\"'/g' | sed -r 's/use href/use xlink:href/g' >'${dir}/${b}'"
		echo "# ${command}"
		eval "${command}"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="SVG Fritzing Fixer" --progress
#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

echo -n "Temporarily Web Server Port: "
read port

if [ "${#port}" -eq "0" ] && [ "${port}" -lt "1024" ]; then
	port="2080"
fi

command="php -S 0.0.0.0:${port}"
echo "${command}"
eval "${command}"

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read -n 1 -p "Press any key to continue..."
echo ""

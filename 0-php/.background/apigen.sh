#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

echo -n "Source Directory ${MY_CANCEL}: "
read source
echo -n "Destination Directory ${MY_CANCEL}: "
read destination

command="echo 'yes' | apigen generate --source '${source}' --destination '${destination}' --title 'php' --charset 'UTF-8' --access-levels 'public' --access-levels 'protected' --php --tree --debug"
echo "${command}"
eval "${command}"

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read -n 1 -p "Press any key to continue..."
echo ""

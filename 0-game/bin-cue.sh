#!/bin/bash

IFS=$'\n'

i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		nf=`basename "${f}"`
		cf=`echo "${nf}" | sed -r 's/(.+)\.[^.]+/\1.cue/g'`
		string="FILE \"${nf}\" BINARY
	TRACK 01 MODE2/2352
		INDEX 01 00:00:00"
		echo "# ${f}"
		echo -e "${string}" >"${cf}"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="Re-Build PSX *.cue for *.bin" --progress
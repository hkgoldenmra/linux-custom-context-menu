#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

for f in `echo "${1}"`; do
	if [ "${#f}" -gt '0' ]; then
		if [ `echo "${f}" | grep '\.iso$'` ]; then
			output="${f:0:-4}.7z"
			command="7z a -mx9 -v1G '${output}' '${f}'"
			echo "${command}"
			eval "${command}"

			Bbin=`du -b "${f}" | awk '{print $1}'`
			MBbin=$(($Bbin/(1000*1000)))
			MiBbin=$(($Bbin/(1024*1024)))
			f=`basename "${f}"`
			echo "${f} - ${Bbin} B, ${MBbin} MB, ${MiBbin} MiB"

			for f in "${output}".*; do
				Bbin=`du -b "${f}" | awk '{print $1}'`
				MBbin=$(($Bbin/(1000*1000)))
				MiBbin=$(($Bbin/(1024*1024)))
				f=`basename "${f}"`
				echo "${f} - ${Bbin} B, ${MBbin} MB, ${MiBbin} MiB"
			done
		fi
	fi
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

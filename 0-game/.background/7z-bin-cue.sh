#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

files=''
namebin=''
namecue=''
for f in `echo "${1}"`; do
	if [ "${#f}" -gt '0' ]; then
		if [ `echo "${f}" | grep '\.bin$'` ]; then
			namebin="${f}"
		elif  [ `echo "${f}" | grep '\.cue$'` ]; then
			namecue="${f}"
		fi
		files="${files} '${f}'"
	fi
done
output="${f:0:-4}.7z"
command="7z a -mx9 '${output}'${files}"
echo "${command}"
eval "${command}"

Bbin=`du -b "${namebin}" | awk '{print $1}'`
MBbin=$(($Bbin/(1000*1000)))
MiBbin=$(($Bbin/(1024*1024)))
namebin=`basename "${namebin}"`
echo "${namebin} - ${Bbin} B, ${MBbin} MB, ${MiBbin} MiB"

Bbin=`du -b "${namecue}" | awk '{print $1}'`
namecue=`basename "${namecue}"`
echo "${namecue} - ${Bbin} B"

Bbin=`du -b "${output}" | awk '{print $1}'`
MBbin=$(($Bbin/(1000*1000)))
MiBbin=$(($Bbin/(1024*1024)))
output=`basename "${output}"`
echo "${output} - ${Bbin} B, ${MBbin} MB, ${MiBbin} MiB"

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

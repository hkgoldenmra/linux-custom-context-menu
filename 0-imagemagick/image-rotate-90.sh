#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

dir="temp"
mkdir "${dir}"

IFS=$'\n'
i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		b=`basename "${f}"`
		command="convert -rotate '90' '${f}' '${dir}/${b}'"
		echo "# ${command}"
		eval "${command}"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="Image Rotate 90" --progress
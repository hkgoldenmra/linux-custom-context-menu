#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

dir="temp"
mkdir "${dir}"

IFS=$'\n'
i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		b=`basename "${f}"`
		commands="convert '${f}' -channel GB -evaluate set 0 '${dir}/${b:-4}[R]${b: -4}'
convert '${f}' -channel RB -evaluate set 0 '${dir}/${b:-4}[G]${b: -4}'
convert '${f}' -channel RG -evaluate set 0 '${dir}/${b:-4}[B]${b: -4}'
convert '${f}' -channel R -evaluate set 0 '${dir}/${b:-4}[C]${b: -4}'
convert '${f}' -channel G -evaluate set 0 '${dir}/${b:-4}[M]${b: -4}'
convert '${f}' -channel B -evaluate set 0 '${dir}/${b:-4}[Y]${b: -4}'"
		for command in `echo "${commands}"`; do
			echo "# ${command}"
			eval "${command}"
		done
	fi
	i=$(($i+1))
done | zenity --width="600" --title="Image Color Channels" --progress
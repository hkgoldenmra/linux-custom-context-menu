#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

size=`zenity \
--width="300" \
--title="Image Resize" \
--text="Initial Settings" \
--add-entry="Width" \
--add-entry="Height" \
--separator="x" \
--forms`

dir="temp"
mkdir "${dir}"

IFS=$'\n'
i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		b=`basename "${f}"`
		command="convert -resize '!${size}' '${f}' '${dir}/${b}'"
		echo "# ${command}"
		eval "${command}"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="Image Resize" --progress
#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

echo -n "Trimming Top [0] ? "
read top
top=`integer "${top}"`
echo -n "Trimming Left [0] ? "
read left
left=`integer "${left}"`
echo -n "Trimming Right [0] ? "
read right
right=`integer "${right}"`
echo -n "Trimming Bottom [0] ? "
read bottom
bottom=`integer "${bottom}"`

dir="temp-dir"
if ! [ -d "${dir}" ]; then
	rm -R "${dir}" 2>/dev/null
	mkdir "${dir}"
fi

for f in `echo "${1}"`; do
	if [ "${#f}" -gt '0' ]; then
		size=`identify "${f}"`
		size=`echo "${size:${#f}}" | awk -F ' ' '{print $2}'`
		width=`echo "${size}" | awk -F 'x' '{print $1}'`
		height=`echo "${size}" | awk -F 'x' '{print $2}'`
		width=$(($width-$left-$right))
		height=$(($height-$top-$bottom))
		if [ "${width}" -gt '0' ] && [ "${height}" -gt '0' ]; then
			dirname=`dirname "${f}"`
			basename=`basename "${f}"`
			command="convert -crop '${width}x${height}+${left}+${top}' +repage '${f}' '${dirname}/${dir}/${basename}'"
			echo "${command}"
			eval "${command}"
		fi
	fi
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

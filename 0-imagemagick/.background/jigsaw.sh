#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

echo -n "How many rows to separate ? "
read rows
echo -n "How many columns to separate ? "
read columns

suffix="jigsaw"
rowsize="${#rows}"
columnsize="${#columns}"

for f in `echo "${1}"`; do
	if [ "${#f}" -gt '0' ]; then
		string=`identify "${f}" 2>/dev/null`
		string="${string: ${#f}}"
		string=`echo "${string}" | sed -r 's/^(.+ )([0-9]+x[0-9]+)( .+)$/\2/g'`
		if [ "${#string}" -gt '0' ]; then
			extension="png"
			mimetype=`file --mime-type "${f}" | awk -F '/' '{print $NF}'`
			if [ `echo "${mimetype}" | grep 'bmp'` ]; then
				extension="bmp"
			elif [ `echo "${mimetype}" | grep 'jpeg'` ]; then
				extension="jpg"
			elif [ `echo "${mimetype}" | grep 'png'` ]; then
				extension="png"
			elif [ `echo "${mimetype}" | grep 'tiff'` ]; then
				extension="tiff"
			elif [ `echo "${mimetype}" | grep 'gif'` ]; then
				extension="gif"
			fi
			rm "${f}.${suffix}"
			mkdir "${f}.${suffix}"
			dirname=`dirname "${f}"`
			basename=`basename "${f}"`

			fwidth=`echo "${string}" | awk -F 'x' '{print $1}'`
			fheight=`echo "${string}" | awk -F 'x' '{print $2}'`
			pwidth=$(($fwidth/$columns))
			pheight=$(($fheight/$rows))

			row='0'
			while [ "${row}" -lt "${rows}" ]; do
				column='0'
				while [ "${column}" -lt "${columns}" ]; do
					srow=`printf "%0${rowsize}d" "${row}"`
					scolumn=`printf "%0${columnsize}d" "${column}"`
					command="convert -crop \"${pwidth}x${pheight}+$(($pwidth*$column))+$(($pheight*$row))\" \"${f}\" \"${f}.${suffix}/${basename}.r${srow}-c${scolumn}.${extension}\""
					echo "${command}"
					eval "${command}"
					column=$(($column+1))
				done
				row=$(($row+1))
			done
		fi
	fi
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

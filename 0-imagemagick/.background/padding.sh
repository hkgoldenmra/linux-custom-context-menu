#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

echo -n "Padding Top [0] ? "
read top
top=`integer "${top}"`
echo -n "Padding Left [0] ? "
read left
left=`integer "${left}"`
echo -n "Padding Right [0] ? "
read right
right=`integer "${right}"`
echo -n "Padding Bottom [0] ? "
read bottom
bottom=`integer "${bottom}"`
echo -n "Padding ARGB Color [000000] ? "
read color
if ! [ `echo "${color}" | grep '[0-9A-Fa-f]\{6,\}'` ]; then
	color='000000'
fi
red="${color:0:2}"
red=$((16#$red))
green="${color:2:2}"
green=$((16#$green))
blue="${color:4:2}"
blue=$((16#$blue))

dir="temp-dir"
if ! [ -d "${dir}" ]; then
	rm -R "${dir}" 2>/dev/null
	mkdir "${dir}"
fi

for f in `echo "${1}"`; do
	if [ "${#f}" -gt '0' ]; then
		size=`identify "${f}" | awk -F ' ' '{print $3}'`
		width=`echo "${size}" | awk -F 'x' '{print $1}'`
		height=`echo "${size}" | awk -F 'x' '{print $2}'`
		if [ "${width}" -gt '0' ] && [ "${height}" -gt '0' ]; then
			dirname=`dirname "${f}"`
			basename=`basename "${f}"`
			command="convert -background 'rgb(${red},${green},${blue})'"
			width=$(($width+$left))
			height=$(($height+$top))
			command="${command} -gravity SouthEast -extent '${width}x${height}' +repage"
			width=$(($width+$right))
			height=$(($height+$bottom))
			command="${command} -gravity NorthWest -extent '${width}x${height}' +repage"
			command="${command} '${f}' '${dirname}/${dir}/${basename}'"
			echo "${command}"
			eval "${command}"
		fi
	fi
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

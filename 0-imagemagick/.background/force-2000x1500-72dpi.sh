#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

dir="temp-dir"
if ! [ -d "${dir}" ]; then
	rm -R "${dir}" 2>/dev/null
	mkdir "${dir}"
fi

for f in `echo "${1}"`; do
	if [ "${#f}" -gt '0' ]; then
		dirname=`dirname "${f}"`
		basename=`basename "${f}"`
		command="convert -resize '!2000x1500' -units 'PixelsPerInch' -density '72' '${f}' '${dirname}/${dir}/${basename}'"
		echo "${command}"
		eval "${command}"
	fi
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

for f in `echo "${1}"`; do
	if [ "${#f}" -gt '0' ]; then
		prefix=`date '+%s' | md5sum | awk '{print $1}'`
		command="mkdir ~/'${prefix}'"
		echo "${command}"
		eval "${command}"
		command="ffmpeg -i '${f}' -r 30 ~/'${prefix}/%02d.png' 2>/dev/null"
		echo "${command}"
		eval "${command}"
		command="convert -delay 2 -dispose background ~/'${prefix}/'*'.png' '${f}.gif'"
		echo "${command}"
		eval "${command}"
		command="rm -R ~/'${prefix}'"
		echo "${command}"
		eval "${command}"
	fi
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

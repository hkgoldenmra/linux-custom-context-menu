#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

command="git add -v ."
echo "${command}"
eval "${command}"

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

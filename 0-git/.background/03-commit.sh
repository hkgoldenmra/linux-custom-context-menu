#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

echo -n "Commit ${MY_CANCEL}: "
read string

command="git commit -m '${string}'"
echo "${command}"
eval "${command}"

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

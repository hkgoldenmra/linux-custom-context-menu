#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

string=`git log`
string=`echo $string | sed -r 's/(commit )/\n\1/g' | sed -r 's/^commit ([^ ]+) Author: (.+) Date: ... (...) (..?) (..:..:..) (....) (.....) (.+)$/\1 \2 \6 \3 \4 \5 \7 \8/g'`
echo "${string:1}"

echo -n "Checkout a Commit ${MY_CANCEL}: "
read string

command="git checkout '${string}'"
echo "${command}"
eval "${command}"

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

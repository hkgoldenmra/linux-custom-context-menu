#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

echo -n "Repository Path ${MY_CANCEL}: "
read string

command="git clone '${string}' --depth=1"
echo "${command}"
eval "${command}"

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

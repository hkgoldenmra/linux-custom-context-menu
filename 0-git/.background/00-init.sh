#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

echo -n "Repository Name ${MY_CANCEL}: "
read string

EXT='.git'
if ! [ "${string: -4}" = "${EXT}" ]; then
	string="${string}${EXT}"
fi

command="git init --bare '${string}'"
echo "${command}"
eval "${command}"

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

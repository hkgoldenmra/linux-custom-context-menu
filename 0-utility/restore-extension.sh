#!/bin/bash

IFS=$'\n'

i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		mime=`file --mime-type -b "${f}"`
		extension=`cat "/etc/mime.types" | grep "^${mime}\s" | sed -r 's/\s/ /g' | sed -r 's/^[^ ]+ +//g'`
		if ! [ "${extension}" = "" ]; then
			count=`echo "${extension}" | awk -F ' ' '{print NF}'`
			if [ "${count}" -gt "1" ]; then
				command=`echo "${extension}" | sed -r 's/ /\n/g'`
				extension=`zenity --width="400" --height="300" --title="Select a file extension" --text="Select a file extension for '${f}'" --column="Possible extension of '${mime}'" $command --list`
			fi
		fi
		if ! [ "${extension}" = "" ]; then
			command="mv '${f}' '${f}.${extension}'"
			echo "# ${command}"
			eval "${command}"
		fi
	fi
	i=$(($i+1))
done | zenity --width="600" --title="Restore Extension" --progress

#!/bin/bash

IFS=$'\n'

now=`date "+%Y-%m-%d-%H-%M-%S-%N"`
mkdir "${now}"
i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		basename=`basename "${f}"`
		command="mv '${f}' '${now}/${basename}'"
		echo "# ${command}"
		eval "${command}"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="To Directory" --progress

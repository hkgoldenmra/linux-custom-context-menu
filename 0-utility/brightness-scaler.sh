#!/bin/bash

ifs=$IFS
IFS=$'\n'
ESCAPE="\\\\"
NEWLINE="\\n"

WIDTH='400'
HEIGHT='250'
TITLE='Brightness Scaler'

MIN_VALUE='1'
MAX_VALUE='10'
ALL_SCREENS='All Screens'

GREP_BRIGHTNESS='.\+Brightness: [0-9]\+\.[0-9]\+'
SED_BRIGHTNESS='s/.+Brightness: ([0-9]+\.[0-9]+)/\1/g'

XRANDR=`xrandr --verbose`
DEFAULT_SCREENS=`echo -n "${XRANDR}" | grep '.\+ connected' | sed -r 's/^(.+) connected.+/\1/g'`
DEFAULT_BRIGHTNESSES=`echo -n "${XRANDR}" | grep "${GREP_BRIGHTNESS}" | sed -r "${SED_BRIGHTNESS}"`

while [ 1 ]; do
	selected_screen=`zenity --list --width="${WIDTH}" --height="${HEIGHT}" --title="${TITLE}" --text="Select a Screen" --column 'List of connected screen' "${ALL_SCREENS}" $(echo -n "${DEFAULT_SCREENS}") 2>/dev/null`
	if [ "${?}" = '0' ]; then
		if [ "${#selected_screen}" -eq '0' ] || [ "${selected_screen}" = "${ALL_SCREENS}" ]; then
			brightness=`echo -n $(echo -n "${DEFAULT_BRIGHTNESSES}" | sed -n '1p') "${MAX_VALUE}" | awk '{print $1 * $2}'`
			screen=`echo -n "${DEFAULT_SCREENS}" | sed -n '1p'`
			zenity --scale --title="${TITLE}" --text="Adjusting ${ALL_SCREENS} Brightness${NEWLINE}Using Screen '${screen}' as default brightness" --min-value="${MIN_VALUE}" --max-value="${MAX_VALUE}" --value="${brightness}" --print-partial 2>/dev/null | while read value; do
				for screen in `echo -n "${DEFAULT_SCREENS}"`; do
					brightness=`echo -n "${value}" "${MAX_VALUE}" | awk '{print $1 / $2}'`
					brightness=`printf '%01.2f' "${brightness}"`
					command="xrandr --output '${screen}' --brightness '${brightness}'"
					eval "${command}"
				done
			done
			if [ "${PIPESTATUS[0]}" = '0' ]; then
				DEFAULT_BRIGHTNESSES=`xrandr --verbose | grep "${GREP_BRIGHTNESS}" | sed -r "${SED_BRIGHTNESS}"`
			else				
				i='1'
				for screen in `echo -n "${DEFAULT_SCREENS}"`; do
					brightness=`echo -n "${DEFAULT_BRIGHTNESSES}" | sed -n "${i}p"`
					command="xrandr --output '${screen}' --brightness '${brightness}'"
					eval "${command}"
					i=$(($i+1))
				done
			fi
		else
			i='1'
			for screen in `echo -n "${DEFAULT_SCREENS}"`; do
				if [ "${selected_screen}" = `echo -n "${DEFAULT_SCREENS}" | sed -n "${i}p"` ]; then break; fi
				i=$(($i+1))
			done
			brightness=`echo -n $(echo "${DEFAULT_BRIGHTNESSES}" | sed -n "${i}p") "${MAX_VALUE}" | awk '{print $1 * $2}'`
			zenity --scale --title="${TITLE}" --text="Adjusting Screen '${selected_screen}' Brightness" --min-value="${MIN_VALUE}" --max-value="${MAX_VALUE}" --value="${brightness}" --print-partial 2>/dev/null | while read value; do
				brightness=`echo -n "${value}" "${MAX_VALUE}" | awk '{print $1 / $2}'`
				brightness=`printf '%01.2f' "${brightness}"`
				command="xrandr --output '${selected_screen}' --brightness '${brightness}'"
				eval "${command}"
			done
			if [ "${PIPESTATUS[0]}" = '0' ]; then
				DEFAULT_BRIGHTNESSES=`xrandr --verbose | grep "${GREP_BRIGHTNESS}" | sed -r "${SED_BRIGHTNESS}"`
			else
				brightness=`echo -n "${DEFAULT_BRIGHTNESSES}" | sed -n "${i}p"`
				command="xrandr --output '${selected_screen}' --brightness '${brightness}'"
				eval "${command}"
			fi
		fi
	else
		break
	fi
done
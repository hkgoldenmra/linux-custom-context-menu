#!/bin/bash

IFS=$'\n'
timestamp=`zenity \
--width="300" \
--title="Rename" \
--text="Initial Settings" \
--add-entry="Prefix" \
--add-entry="Suffix" \
--add-entry="Start Index" \
--add-entry="Length" \
--separator="|" \
--forms-date-format="%Y-%m-%d" --forms`

if [ "${?}" = "0" ]; then
	prefix=`echo "${timestamp}" | awk -F '|' '{print $1}'`
	suffix=`echo "${timestamp}" | awk -F '|' '{print $2}'`
	start=`echo "${timestamp}" | awk -F '|' '{print $3}'`
	start=$(($start+0))
	length=`echo "${timestamp}" | awk -F '|' '{print $4}'`
	length=$(($length+0))
	i="0"
	for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
		echo "100 * ${i} / ${#}" | bc
		if [ "${f}" = "" ]; then
			echo "# Complete !!!"
		else
			pad_start=`printf "%0${length}d" "${start}"`
			dir=`dirname "${f}"`
			extension=`echo "${f}" | sed -r 's/.+\.([^.]+)$/\1/g'`
			command="mv '${f}' '${dir}/${prefix}${pad_start}${suffix}.${extension}'"
			echo "# ${command}"
			eval "${command}"
		fi
		start=$(($start+1))
		i=$(($i+1))
	done | zenity --width="600" --title="Rename" --progress
fi

#!/bin/bash

IFS=$'\n'
timestamp=`zenity \
--width="300" \
--title="Re-Generate Orders" \
--text="Initial Settings" \
--add-calendar="Date (YYYY-MM-DD)" \
--add-entry="Time (HH:MM)" \
--separator=" " \
--forms-date-format="%Y-%m-%d" \
--forms`

if [ "${?}" = "0" ]; then
	timestamp=`date -d "${timestamp}:00" "+%s"`
	i="0"
	for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
		echo "100 * ${i} / ${#}" | bc
		if [ "${f}" = "" ]; then
			echo "# Complete !!!"
		else
			datetime=`date -d "@${timestamp}" "+%Y-%m-%d %H:%M:00"`
			command="touch -d '${datetime}' '${f}'"
			echo "# ${command}"
			eval "${command}"
			timestamp=`echo "${timestamp} + 60" | bc`
		fi
		i=$(($i+1))
	done | zenity --width="600" --title="Re-Generate Orders" --progress
fi

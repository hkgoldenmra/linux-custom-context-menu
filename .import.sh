#!/bin/bash

my_service(){
	command="service '${1}' '${2}'"
	echo "${command}"
	eval "${command}"
}

integer(){
	r="${1}"
	if [ `echo "${r}" | grep '[0-9]\+'` ]; then
		r=$((10#$r))
		if [ "${r}" -le '0' ]; then
			r='0'
		fi
	else
		r='0'
	fi
	echo "${r}"
}

MY_TERMINAL='mate-terminal -x'
MY_CANCEL='(Press "Ctrl C" to cancel)'
MY_COMPLETE='Completed!!!'
MY_START_DATETIME=`date '+%s'`

realpath=`readlink -f "${0}"`
dirname=`dirname "${realpath}"`
basename=`basename "${realpath}"`

#!/bin/bash

IFS=$'\n'

i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		bf=`basename "${f}"`
		bf="/tmp/${bf}-z"
		echo "# Creating Audio File '${f}.mp3'"
		c="0"
		for wl in `cat "${f}"`; do
			for l in `echo "${wl}" | sed -r 's/(.{1,100})/\1\n/g'`; do
				c=$(($c+1))
			done
		done
		j="0"
		echo -n "" >"${bf}-z.txt"
		for wl in `cat "${f}"`; do
			for l in `echo "${wl}" | sed -r 's/(.{1,100})/\1\n/g'`; do
				echo "100 * ${j} / ${c}" | bc
				nf=`printf "%s-%09d.mp3" "${bf}" "${j}"`
				command="wget \
--header='Referer: https://inforobo.com/' \
'https://www.defit.org/gv.php?t=${l}&tl=zh-HK&sv=g1&vn=&pitch=0.5&rate=0.5&vol=1' -O '${nf}'"
				echo "file '${nf}'" >>"${bf}.txt"
				echo "# ${command}"
				eval "${command}"
				j=$(($j+1))
			done
		done | zenity --width="600" --title="Downloading Audios" --progress --auto-close
		ffmpeg -f "concat" -safe "0" -i "${bf}.txt" -acodec "libmp3lame" -ab "32k" -ar "22050" -ac "1" "${f}.mp3"
		rm "${bf}-"*".mp3" "${bf}.txt"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="Inforobo Online Text To Speech" --progress

#!/bin/bash

# check required commands
for f in "curl" "jq"; do
	eval "${f} --help"
	if ! [ "${?}" = "0" ]; then
		zenity --width="600" --text="'${f}' command is required" --error
		exit
	fi
done

IFS=$'\n'

default_id="546c25a59c58ad7"
temp="imgur.com-upload.log"
base_url="https://api.imgur.com"
upload_url="${base_url}/3/upload"
delete_url="${base_url}/3/image"

echo -n "" >"${temp}"
i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		mime=`file --mime-type --brief "${f}"`
		command="curl -sL --request 'POST' --header 'Authorization: Client-ID ${default_id}' --form 'image=@\"${f}\"' '${upload_url}'"
		echo "# ${command/${default_id}/*}"
		response=`eval "${command}"`
		link=`echo "${response}" | jq -r ".data.link"`
		deletehash=`echo "${response}" | jq -r ".data.deletehash"`
		echo "x-www-browser '${link}'" >"${f}-open.sh"
		echo "curl -sL --request 'DELETE' --header 'Authorization: Client-ID ${default_id}' '${delete_url}/${deletehash}'
echo \"\${0}\"
rm \"\${0}\"" >"${f}-delete.sh"
		chmod +x "${f}-open.sh"
		chmod +x "${f}-delete.sh"
		if [ "${deletehash}" = "" ] || [ "${deletehash}" = "null" ]; then
			echo "[-] '${f}'" >>"${temp}"
		else
			echo "[+] '${f}' '${link}'" >>"${temp}"
		fi
	fi
	i=$(($i+1))
done | zenity --width="600" --title="Uploading Media to imgur.com" --progress

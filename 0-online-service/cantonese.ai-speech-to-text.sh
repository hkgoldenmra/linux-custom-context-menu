#!/bin/bash

# check required commands
for f in "curl" "awk" "jq"; do
	if ! [ `command -v "${f}"` ]; then
		zenity --width="600" --text="'${f}' command is required" --error
		exit
	fi
done

IFS=$'\n'

echo -n "" >"${temp}"
i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		command="curl --request 'POST' --form 'data=@''${f}' 'https://ios-shortcut-transcribe-api.cantonese.ai' | jq -r '.text' | awk -F '                                                                                                                                                                                                                            ----------------------------------------' '{print \$1}'"
		echo "# ${command}"
		eval "${command}" >"${f}.txt"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="Speech To Text" --progress

#!/bin/bash

IFS=$'\n'
package="hk.com.redso.read4u"
certification="68f82aa766767ce1ac85a08b159c2fef19501f28"
api_key="AIzaSyC0lqNoLu8VtLMS_2Z0E5Ma-zuKGBxjgO0"

i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		file="/tmp/"`basename "${f}"`".json"
		echo -n '{"requests":[{"features":[{"type":"DOCUMENT_TEXT_DETECTION"}],"image":{"content":"'`cat "${f}" | base64 -w 1024`'"}}]}' >"${file}"
		command="curl -sL \
--request 'POST' \
--header 'X-Android-Package: ${package}' \
--header 'X-Android-Cert: ${certification}' \
--header 'Content-Type: application/json; charset=UTF-8' \
--data @'${file}' \
'https://vision.googleapis.com/v1/images:annotate?key=${api_key}' | \
jq '.responses[].textAnnotations[].description' | \
head -1 | \
jq -r"
temp="${command}"
temp="${temp/${package}/Package}"
temp="${temp/${certification}/Certification}"
temp="${temp/${api_key}/API Key}"
		echo "# ${temp}"
		eval "${command}" >"${f}.txt"
		rm "${file}"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="Google OCR" --progress

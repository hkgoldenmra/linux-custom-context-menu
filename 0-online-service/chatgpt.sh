#!/bin/bash

IFS=$'\n'

template='{"messages":[{"role":"user","content":"%s"}]}'
message=`zenity --entry --width="600" --title="ChatGPT"`
if [ "${?}" = "0" ]; then
	template=`printf "${template}" "${message}"`
	result=`curl \
		--request "POST" \
		--header "Content-Type: application/json; charset=UTF-8" \
		--data "${template}" \
		'https://chatgpt-git-main-sdgfhd.vercel.app/api/stream'`
	file=`mktemp`
	echo -n "${message}${result}" >"${file}"
	zenity --text-info --filename="${file}" --width="600" --height="400" --title="ChatGPT" --cancel-label="Save"
	if [ "${?}" = "1" ]; then
		out=`zenity --file-selection --title="ChatGPT" --save --filename="${message}.txt" --confirm-overwrite --file-filter="*.txt"`
		echo -n "${message}${result}" >"${out}"
	fi
	rm "${file}"
fi
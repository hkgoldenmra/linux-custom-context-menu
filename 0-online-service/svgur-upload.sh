#!/bin/bash

# check required commands
for f in "curl"; do
	eval "${f} --help"
	if ! [ "${?}" = "0" ]; then
		zenity --width="600" --text="'${f}' command is required" --error
		exit
	fi
done

IFS=$'\n'

echo -n "" >"${temp}"
i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		url=`curl -sL --request "GET" "https://svgur.com/" | grep 'https:..svgur.com._ah.upload.' | sed -r 's/.+action="([^"]+)".+/\1/g'`
		command="curl --request 'POST' --form 'file=@\"${f}\"' --form 'name=`basename "${f}"`' --form 'submit=Share' '${url}'"
		echo "# ${command}"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="Uploading SVG files to svgur.com" --progress

#!/bin/bash

urlencode(){
	length="${#1}"
	i="0"
	while [ "${i}" -lt "${length}" ]; do
		c="${1:$i:1}"
		printf "${c}" | \
		xxd -p -c1 | \
		while read x; do
			printf "%%%s" "${x}"
		done
		i=$(($i+1))
	done
}

IFS=$'\n'
WIDTH='400'
HEIGHT='400'
TITLE='Translator'

CODE_LANGUAGES="'af-ZA' 'Afrikaans' \
'am-ET' 'አማርኛ' \
'az-AZ' 'Azərbaycanca' \
'bn-BD' 'বাংলাদেশ' \
'bn-IN' 'ভারত' \
'id-ID' 'Bahasa Indonesia' \
'ms-MY' 'Bahasa Melayu' \
'ca-ES' 'Català' \
'cs-CZ' 'Čeština' \
'da-DK' 'Dansk' \
'de-DE' 'Deutsch' \
'en-AU' 'Australia' \
'en-CA' 'Canada' \
'en-IN' 'India' \
'en-KE' 'Kenya' \
'en-TZ' 'Tanzania' \
'en-GH' 'Ghana' \
'en-NZ' 'New Zealand' \
'en-NG' 'Nigeria' \
'en-ZA' 'South Africa' \
'en-PH' 'Philippines' \
'en-GB' 'United Kingdom' \
'en-US' 'United States' \
'es-AR' 'Argentina' \
'es-BO' 'Bolivia' \
'es-CL' 'Chile' \
'es-CO' 'Colombia' \
'es-CR' 'Costa Rica' \
'es-EC' 'Ecuador' \
'es-SV' 'El Salvador' \
'es-ES' 'España' \
'es-US' 'Estados Unidos' \
'es-GT' 'Guatemala' \
'es-HN' 'Honduras' \
'es-MX' 'México' \
'es-NI' 'Nicaragua' \
'es-PA' 'Panamá' \
'es-PY' 'Paraguay' \
'es-PE' 'Perú' \
'es-PR' 'Puerto Rico' \
'es-DO' 'República Dominicana' \
'es-UY' 'Uruguay' \
'es-VE' 'Venezuela' \
'eu-ES' 'Euskara' \
'fil-PH' 'Filipino' \
'fr-FR' 'Français' \
'jv-ID' 'Basa Jawa' \
'gl-ES' 'Galego' \
'gu-IN' 'ગુજરાતી' \
'hr-HR' 'Hrvatski' \
'zu-ZA' 'IsiZulu' \
'is-IS' 'Íslenska' \
'it-IT' 'Italia' \
'it-CH' 'Svizzera' \
'kn-IN' 'ಕನ್ನಡ' \
'km-KH' 'ភាសាខ្មែរ' \
'lv-LV' 'Latviešu' \
'lt-LT' 'Lietuvių' \
'ml-IN' 'മലയാളം' \
'mr-IN' 'मराठी' \
'hu-HU' 'Magyar' \
'lo-LA' 'ລາວ' \
'nl-NL' 'Nederlands' \
'ne-NP' 'नेपाली भाषा' \
'nb-NO' 'Norsk bokmål' \
'pl-PL' 'Polski' \
'pt-BR' 'Brasil' \
'pt-PT' 'Portugal' \
'ro-RO' 'Română' \
'si-LK' 'සිංහල' \
'sl-SI' 'Slovenščina' \
'su-ID' 'Basa Sunda' \
'sk-SK' 'Slovenčina' \
'fi-FI' 'Suomi' \
'sv-SE' 'Svenska' \
'pt-BR' 'Brasil' \
'pt-PT' 'Portugal' \
'ka-GE' 'ქართული' \
'hy-AM' 'Հայերեն' \
'ta-IN' 'இந்தியா' \
'ta-SG' 'சிங்கப்பூர்' \
'ta-LK' 'இலங்கை' \
'ta-MY' 'மலேசியா' \
'te-IN' 'తెలుగు' \
'vi-VN' 'Tiếng Việt' \
'tr-TR' 'Türkçe' \
'ur-PK' 'پاکستان' \
'ur-IN' 'بھارت' \
'el-GR' 'Ελληνικά' \
'bg-BG' 'български' \
'ru-RU' 'Pусский' \
'sr-RS' 'Српски' \
'uk-UA' 'Українська' \
'ko-KR' '한국어' \
'zh-CN' '中国普通话' \
'zh-HK' '香港普通话' \
'zh-TW' '台灣國語' \
'zh-HK' '香港粵語' \
'ja-JP' '日本語' \
'hi-IN' 'हिन्दी' \
'th-TH' 'ภาษาไทย'"

method="GET"
userAgentName="UnknownTranslator"
userAgentVersion="0.0.1"
encoding="UTF-8"
#sourceLanguage="en-US"
#targetLanguage="zh-HK"
protocol="https"
domain="translate.google.com"
path="/translate_a/t"

command="zenity --list --width='${WIDTH}' --height='${HEIGHT}' --title='${TITLE}' --column='Code' --column='Language' ${CODE_LANGUAGES}"
sourceLanguage=`eval "${command} --text='Select Source Language'"`
targetLanguage=`eval "${command} --text='Select Target Language'"`

i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		text=`cat "${f}"`
		text=`urlencode "${text}"`
		command="curl -sL \
--request '${method}' \
--header 'User-Agent: ${userAgentName}/${userAgentVersion}' \
'${protocol}://${domain}${path}\
?client=at\
&ie=${encoding}\
&oe=${encoding}\
&sl=${sourceLanguage}\
&tl=${targetLanguage}\
&text=${text}' | \
jq -r '.sentences[].trans'"
		nf="${f}-${sourceLanguage}-${targetLanguage}.txt"
		echo "# ${f} ${sourceLanguage} -&gt; ${targetLanguage}"
		eval "${command}" >"${f}-${sourceLanguage}-${targetLanguage}.txt"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="Google Translator" --progress
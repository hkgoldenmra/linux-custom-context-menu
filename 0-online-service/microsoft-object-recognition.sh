#!/bin/bash

IFS=$'\n'
api_key="9fafad413c0240d8a5e49b5c7f95ad59"

i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		mime=`file --mime-type --brief "${f}"`
		command="curl -sL \
--request 'POST' \
--header 'Ocp-Apim-Subscription-Key: ${api_key}' \
--header 'Content-Type: ${mime}' \
--data-binary @'${f}' \
'https://eastasia.api.cognitive.microsoft.com/vision/v1.0/analyze?visualFeatures=description,tags&details=celebrities,landmarks&language=zh' \
| jq '.tags[]' \
| jq -r ['.name, (((.confidence * 10000) % 10000) / 100 | tostring) + \"%\"] | @tsv'"
temp="${command}"
temp="${temp/${api_key}/API Key}"
		echo "# ${temp}"
		eval "${command}" >"${f}.txt"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="Microsoft Object Recognition" --progress

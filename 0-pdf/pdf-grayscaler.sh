#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

dir="temp"
mkdir "${dir}"

IFS=$'\n'
i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		b=`basename "${f}"`
		command="gs -d'NOPAUSE' -d'BATCH' -s'DEVICE'='pdfwrite' -s'ColorConversionStrategy'='Gray' -s'OutputFile'='${dir}/${b}' '${f}'"
		echo "# ${command}"
		eval "${command}"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="PDF Grayscaler" --progress
#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

password0=`zenity --password --width="600" --title="PDF Encrypter | Enter Password"`
if [ "${?}" = "1" ]; then
	exit
fi
password1=`zenity --password --width="600" --title="PDF Encrypter | Confirm Password"`
if [ "${?}" = "1" ]; then
	exit
fi
if ! [ "${password0}" = "${password1}" ]; then
	zenity --error --text="Password not matched" --title="PDF Encrypter" 
	exit
fi

dir="temp"
mkdir "${dir}"

i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		b=`basename "${f}"`
		echo "# gs -d'NOPAUSE' -d'BATCH' -s'DEVICE'='pdfwrite' -s'OwnerPassword'='*****' -s'UserPassword'='*****' -s'OutputFile'='${dir}/${b}' '${f}'"
		eval "gs -d'NOPAUSE' -d'BATCH' -s'DEVICE'='pdfwrite' -s'OwnerPassword'='${password0}' -s'UserPassword'='${password0}' -s'OutputFile'='${dir}/${b}' '${f}'"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="PDF Encrypter" --progress
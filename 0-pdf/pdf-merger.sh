#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

dir="temp"
mkdir "${dir}"

IFS=$'\n'
i="0"
fs=""
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		fs="${fs} '${f}'"
	fi
	i=$(($i+1))
done

if [ "1" ]; then
	command="gs -d'NOPAUSE' -d'BATCH' -s'DEVICE'='pdfwrite' -s'OutputFile'='${dir}/merged.pdf' ${fs}"
	echo "# ${command}"
	eval "${command}"
fi | zenity --width="600" --title="PDF Merger" --progress
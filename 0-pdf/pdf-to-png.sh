#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

dir="temp"
mkdir "${dir}"

temp="/tmp/temp.pdf"

IFS=$'\n'
i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		b=`basename "${f}"`
		gs -d'NOPAUSE' -d'BATCH' -s'DEVICE'='pdfwrite' -s'OutputFile'="${temp}" "${f}"
		count=`cat "${temp}" | grep --text '/Count' | sed -r 's/.*\/Count ([0-9]+).*/\1/g'`
		rm "${temp}"
		command="gs -d'NOPAUSE' -d'BATCH' -s'DEVICE'='png16m' -s'OutputFile'='${dir}/${b:0:-4}[%0${#count}d].png' '${f}'"
		echo "# ${command}"
		eval "${command}"
	fi
	i=$(($i+1))
done | zenity --width="600" --title="PDF Splitter" --progress
#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

dir="temp"
mkdir "${dir}"

i="0"
for f in `echo "${CAJA_SCRIPT_SELECTED_FILE_PATHS}"` ""; do
	echo "100 * ${i} / ${#}" | bc
	if [ "${f}" = "" ]; then
		echo "# Complete !!!"
	else
		password=`zenity --password --width="600" --title="PDF Decrypter | Enter Password"`
		if [ "${?}" = "1" ]; then
			password=""
		else
			b=`basename "${f}"`
			echo "# gs -d'NOPAUSE' -d'BATCH' -s'DEVICE'='pdfwrite' -s'PDFPassword'='*****' -s'OutputFile'='${dir}/${b}' '${f}'"
			eval "gs -d'NOPAUSE' -d'BATCH' -s'DEVICE'='pdfwrite' -s'PDFPassword'='${password}' -s'OutputFile'='${dir}/${b}' '${f}'"
			if [ "${?}" = "1" ]; then
				rm "${dir}/${b}"
				zenity --error --text="Incorrect password - cannot decrypt PDF file \"${f}\""
			fi
		fi
	fi
	i=$(($i+1))
done | zenity --width="600" --title="PDF Decrypter" --progress
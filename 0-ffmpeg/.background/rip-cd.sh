#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

info=`mplayer -cdrom-device '/dev/sr0' 'cdda://' -frames 0 -identify 2>/dev/null`
tracks=`echo "${info}" | grep 'ID_CDDA_TRACKS=[0-9]\+' | awk -F '=' '{print $2}'`
track="0"
while [ "${track}" -lt "${tracks}" ]; do
	track=$(($track+1))
	t=`printf "%0${#tracks}d" "${track}"`
	command="mplayer -cdrom-device '/dev/sr0' 'cdda://${track}' -dumpstream -dumpfile 'cdda-${t}.wav' 2>/dev/null"
	echo "${command}"
	eval "${command}"
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

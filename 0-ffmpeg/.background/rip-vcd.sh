#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

info=`mplayer -cdrom-device '/dev/sr0' 'vcd://' -frames 0 -identify 2>/dev/null`
start=`echo "${info}" | grep 'ID_VCD_START_TRACK=[0-9]\+' | awk -F '=' '{print $2}'`
end=`echo "${info}" | grep 'ID_VCD_END_TRACK=[0-9]\+' | awk -F '=' '{print $2}'`

while [ "${start}" -le "${end}" ]; do
	s=`printf "%0${#end}d" "${start}"`
	command="mplayer -cdrom-device '/dev/sr0' 'vcd://${start}' -dumpstream -dumpfile 'vcd-${start}.mpeg' 2>/dev/null"
	echo "${command}"
	eval "${command}"
	start=$(($start+1))
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

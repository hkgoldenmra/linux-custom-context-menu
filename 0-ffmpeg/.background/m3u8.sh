#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

echo -n "m3u8 prefix: "
read prefix

for f in `echo "${1}"`; do
	if [ "${#f}" -gt '0' ]; then
		replace=`echo "${prefix}" | sed -r 's/^(.+)\/.*$/\1/g' | sed -r 's/\//\\\\\//g'`
		cat "${f}" | grep '^.\+\.ts$' | sed -r 's/^(.+\.ts)$/'"${replace}"'\/\1/g' >"${f}.txt"
		wget -i "${f}.txt"
		ffmpeg -y -i "${f}" -vcodec libx264 -vb 1600k -acodec libmp3lame -ab 128k -ar 44100 -ac 2 -map 0:v:0 -map 0:a:0 -map_metadata -1 "${f}.mp4"
	fi
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

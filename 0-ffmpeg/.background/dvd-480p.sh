#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

for f in `echo "${1}"`; do
	if [ "${#f}" -gt '0' ]; then
		command="ffmpeg -y -i '${f}' -s 720x480 -vcodec mpeg2video -vb 1200k -acodec libmp3lame -ab 128k -ar 44100 -ac 2 -map_metadata -1 '${f}.dvd-480p.mpeg'"
		echo "${command}"
		eval "${command}"
	fi
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

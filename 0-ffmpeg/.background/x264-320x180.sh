#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

IFS=$'\n'

for f in `echo "${1}"`; do
	if [ "${#f}" -gt '0' ]; then
		command="ffmpeg -y -i '${f}' -preset ultrafast -s 320x180 -vcodec libx264 -vb 256k -acodec libmp3lame -ab 64k -ar 44100 -ac 1 -map_metadata -1 '${f}.x264-320x180.mp4'"
		echo "${command}"
		eval "${command}"
	fi
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

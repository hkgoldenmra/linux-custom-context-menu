#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

info=`mplayer -dvd-device "/dev/sr0" "dvd://" -frames 0 -identify 2>/dev/null`
titles=`echo "${info}" | grep '^ID_DVD_TITLES=[0-6]\+$' | awk -F '=' '{print $2}'`
title="0"
while [ "${title}" -lt "${titles}" ]; do
	title=$(($title+1))
	chapters=`echo "${info}" | grep 'ID_DVD_TITLE_'"${title}"'_CHAPTERS=[0-9]\+' | awk -F '=' '{print $2}'`
	chapter="0"
	while [ "${chapter}" -lt "${chapters}" ]; do
		chapter=$(($chapter+1))
		t=`printf "%0${#title}d" "${title}"`
		c=`printf "%0${#chapter}d" "${chapter}"`
		command="mplayer -dvd-device '/dev/sr0' 'dvd://${title}' -chapter '${chapter}-${chapter}' -dumpstream -dumpfile 'dvd-${t}-${c}.vob' 2>/dev/null"
		echo "${command}"
		eval "${command}"
	done
done

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read

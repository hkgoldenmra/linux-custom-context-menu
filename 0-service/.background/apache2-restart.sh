#!/bin/bash

source "${HOME}/.config/caja/scripts/.import.sh"

my_service "apache2" "restart"

end=`date '+%s'`
duration=$(($end-$MY_START_DATETIME))

echo "Time used: ${duration} seconds"
echo "${MY_COMPLETE}"
read
